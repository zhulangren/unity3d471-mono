package main

import (
"bufio"
"crypto/md5"
"encoding/hex"
"io/ioutil"
"fmt"
"os"
"path/filepath"
"container/list"
"strings"
)
var outputFileName string = "filesName.txt"

func CheckErr(err error) {
	if nil != err {
	panic(err)
}
}

func GetFullPath(path string) string {
	absolutePath, _ := filepath.Abs(path)
	return absolutePath
}

func byte2string(in [16]byte) []byte {
    tmp := make([]byte, 16)
    for _, value := range in {
        tmp = append(tmp, value)
    }
 
    return tmp[16:]
}

func MakeMd5(path string ) string {
    fileName := path
    fin, err := os.OpenFile(fileName, os.O_RDONLY, 0644)
    if err != nil {
        fmt.Println(fileName, err)
    }
    defer fin.Close()
    Buf, buferr := ioutil.ReadFile(fileName)
    if buferr != nil {
        fmt.Println(fileName, buferr)
    }
    temp := hex.EncodeToString(byte2string(md5.Sum(Buf))) 
    fmt.Printf("md5sum = %s\n", temp)
    return temp
}



func PrintFilesName(path string) {
	fullPath := GetFullPath(path)

	listStr := list.New()

	filepath.Walk(fullPath, func(path string, fi os.FileInfo, err error) error {
		if nil == fi {
			return err
		}
		if fi.IsDir() {
			return nil
		}
		
		name := fi.Name()
		if strings.HasSuffix(name, "meta") {
			return nil
		}

		name="\""+ name +"\":\"" + MakeMd5(path)+"\""
		if outputFileName != name{
			listStr.PushBack(name)
		}
		return nil
	})

	OutputFilesName(listStr)
}

func ConvertToSlice(listStr *list.List)[]string{
	sli := []string{}
	for el:= listStr.Front(); nil != el; el= el.Next(){
		sli = append(sli, el.Value.(string))
	}

	return sli
}

func OutputFilesName(listStr *list.List) {
	files := ConvertToSlice(listStr)
	//sort.StringSlice(files).Sort()// sort  

	f, err := os.Create(outputFileName)
	//f, err := os.OpenFile(outputFileName, os.O_APPEND | os.O_CREATE, os.ModeAppend)
	CheckErr(err)
	defer f.Close()

	f.WriteString("[")
	writer := bufio.NewWriter(f)

	length := len(files)
	for i:= 0; i < length-1; i++{
		writer.WriteString(files[i]+",")
	}
	writer.WriteString(files[length-1]+"]")
	writer.Flush()
	f.Close()
}

func main() {
	var path string

	if len(os.Args) > 2 {
		path = os.Args[1]
		outputFileName=os.Args[2]
	}else if len(os.Args) > 1 {
		path = os.Args[1]
	} else {
		path, _ = os.Getwd()
	}
	PrintFilesName(path)
	fmt.Println("done!")
}
