package main

import (
	"bytes"
	"crypto/rc4"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func main() {
	arg_num := len(os.Args)
	if arg_num < 2 {
		fmt.Printf("Usage: %s [src]\n", os.Args[0])
		os.Exit(0)
	}
	srcfile := os.Args[1]

	file, err := os.OpenFile(srcfile, os.O_RDONLY, os.ModePerm)
	if err != nil {
		defer file.Close()
		fmt.Printf("file open faild\n")
		os.Exit(0)
	}
	stat, e := file.Stat()
	if e != nil {
		fmt.Printf("file read faild\n")
		os.Exit(0)
	}

	size := stat.Size()
	fmt.Printf("Success Open File %d\n", size)
	var buffer bytes.Buffer
	io.CopyN(&buffer, file, size)
	_bytes := buffer.Bytes()

	defer file.Close()

	//重命名源文件
	ioutil.WriteFile(srcfile+".bak", _bytes, 0644)

	if _bytes[0] != 0x4d && _bytes[1] != 0x5a {
		fmt.Printf("error file format %X,%X\n", _bytes[0], _bytes[1])
		os.Exit(0)
	}

	key := []byte{0x96, 0xFE, 0xA5, 0xC8, 0x19, 0x29, 0xD3, 0x7F, 0x40, 0x7E, 0x6B}
	c, err := rc4.NewCipher(key)
	dst := make([]byte, len(_bytes))
	c.XORKeyStream(dst, _bytes)

	ioutil.WriteFile(srcfile, dst, 0644)

}
