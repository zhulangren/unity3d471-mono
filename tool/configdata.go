package main

import (
	"bytes"
	"crypto/rc4"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strconv"
)

func main() {

	arg_num := len(os.Args)
	if arg_num < 2 {
		fmt.Printf("Usage: %s [src]\n", os.Args[0])
		os.Exit(0)
	}
	srcfile := os.Args[1]

	file, err := os.OpenFile(srcfile, os.O_RDONLY, os.ModePerm)
	if err != nil {
		defer file.Close()
		fmt.Printf("file open faild\n")
		os.Exit(0)
	}
	stat, e := file.Stat()
	if e != nil {
		fmt.Printf("file read faild\n")
		os.Exit(0)
	}

	size := stat.Size()
	fmt.Printf("Success Open File %d\n", size)
	var buffer bytes.Buffer
	io.CopyN(&buffer, file, size)
	_bytes := buffer.Bytes()

	defer file.Close()

	//重命名源文件
	ioutil.WriteFile(srcfile+".bak", _bytes, 0644)

	if _bytes[0] != 0x55 && _bytes[1] != 0x6e {
		fmt.Printf("error file format %X,%X\n", _bytes[0], _bytes[1])
		os.Exit(0)
	}

	var byte_privite string = "00276eb4e2bf4c6b83090a9ab6bddecb7e5f4a3a3b501a173d74e055f1d997df5c0840b833ef234f9012eea2f4d7f2ab26494b87af8bd5eac616e634c4ed7d52484342476572584138648ef7fb9f530fd168f896ad67959293229982d4a72a251b7710c96a390771acbc75d0815e4d61a0448c86ce5d2d791e5bfd7b919e190d9bc0bbb20e243511a3782ebeaefc7cb07618e12c36b91f7337e3e97fd26f45e420880ceba43ff3c1281cdb30c2017a573e5921f0b5da1d05ccd656b380c89c5aecd8e76dca84cd89608f03a8ddf5cf15e83262c3a9069431a5048af9dc98292f8d85fa519de5fe46aaba6913d36c02c72b63c5a13c660bff54f6b7b170144ea6"
	byte_privites := []rune(byte_privite)
	length := len(byte_privites) / 2
	var mybyte []byte
	for i := 0; i < length; i++ {
		k, err := strconv.ParseInt(string(byte_privites[i*2:i*2+2]), 16, 32)
		if err != nil {
			panic(err)
		}
		mybyte = append(mybyte, byte(k))
	}
	fmt.Printf("aaaaaaaa:%d\n", len(mybyte))
	c, err := rc4.NewCipher(mybyte)
	dst := make([]byte, len(_bytes))
	c.XORKeyStream(dst, _bytes)

	ioutil.WriteFile(srcfile, dst, 0644)

}
